__author__ = 'asm'


class Cat:
    def __init__(self, name):
        self.name = name

    def voice(self):
        return "meow"


myCat = Cat("kiisa")
print("Cat's name is: "+myCat.name)
print(myCat.name+" can say "+myCat.voice())

