__author__ = 'muravya'
# One var
name = 'Jyrki'
print("Hello, %s!" % name)

# 2+ args
age = 24
print "%s is %d years old. " % (name, age)

# Printing list
tList = [1, 2, 3]
print("A T-list is: %s" % tList)

# Floats
fNum = 3462.8632
print("Float as is: %f" % fNum)
print("Float with 2 digits (like currency) only: %.2f" % fNum )
print("Float as int: %d" % fNum)

# Hex
iNum = 777
hNum = 0xDEADCAFE

print "%d is %X in HEX" % (iNum, iNum)
print "%X is %d in Dec" % (hNum, hNum)
