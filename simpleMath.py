__author__ = 'muravya'

rslt = 1 + 2 - 3 * 4 / 4.0
print(rslt)  # will print 0.0 (because of cast to float) obviously

#modulo
remainder = 11 % 3
print(remainder)

#power
square = 4 ** 2  # =16
cubed = 2 ** 3   # =8

#* operator is overridden for Strings
dopleHertz = "Hertz"*2
print(dopleHertz)

#+ operator is overridden for Lists
list1 = [1, 11, 111]
list2 = [2, 22, 222]

jointList = list1+list2
print("Joint List")
for i in jointList:
    print(i)
#+ operator is overridden for Lists
multiList = list1 * 5
print ("List x5")
for i in multiList:
    print(i)





