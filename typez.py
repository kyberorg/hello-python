#origin name was types.py, but types is reserved class at Python StdLib
__author__ = 'muravya'

# simple number
myInt = 7
myFloat = 5.3
myFloat = float(4.4)

# inline declaration
a, b = 3, 4

# Strings
myStr0 = 'hello'
myStr = "Jim's Cafe"
# Concatenation
helloFromCafe = myStr0 + ' from ' + myStr
print(helloFromCafe)
# Cannot mix types
#This:
# print(helloFromCafe + a)
# causes:
# TypeError: cannot concatenate 'str' and 'int' objects

# Lists
myList = []
myList.append(1)
myList.append(2)

yetAnotherList = [1, 2]  # gives same result as myList

print(myList[1])  # print 2
# iteration
print("Iteration thru list is very Easy.")
for x in myList:
    print(x)

#Dictionary (Map)
myMap = {"name": "SuperMap", "capacity": -1, "mutable": True, "canStoreLists": True, "list": myList}

if myMap.__contains__("list"):
    print("myMap[\"list\"]: %d" % myMap["list"][1])
else:
    print(myMap["name"]+" doesn't have key named 'list'")

#Tuple (immutable list)
myTuple = ('abc', 'def')
print(myTuple[1])  # prints 'def'
#myTuple[1]='fed' - will produce error

